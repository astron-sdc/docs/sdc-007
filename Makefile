DOCNAME=SDC-005
export TEXMFHOME ?= astron-texmf/texmf

GITVERSION := $(shell git log -1 --date=short --pretty=%h)
GITDATE := $(shell git log -1 --date=short --pretty=%ad)
GITSTATUS := $(shell git status --porcelain)
ifneq "$(GITSTATUS)" ""
	GITDIRTY = -dirty
endif

$(DOCNAME).pdf: $(DOCNAME).tex meta.tex
	xelatex $(DOCNAME)
	makeglossaries $(DOCNAME)
	xelatex $(DOCNAME)

.FORCE:

meta.tex: Makefile .FORCE
	rm -f $@
	printf "%s\n" "% GENERATED FILE -- edit this in the Makefile" >>$@
	printf "%s\n" "\newcommand{\vcsRevision}{$(GITVERSION)$(GITDIRTY)}" >>$@
	printf "%s\n" "\newcommand{\vcsDate}{$(GITDATE)}" >>$@
